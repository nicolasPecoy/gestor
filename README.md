Materia - Fundamentos de Ingeniería de Software - Obligatorio 1

Código grupo N3A - Docentes: Pablo Lucini, Gabriela Sánchez

Estudiantes del equipo: Nicolás Pecoy (192991)

Fecha de entrega: 11/05/2020

URL del repositorio: https://bitbucket.org/nicolasPecoy/gestor/src/master/

 # Obligatorio 1 Fundamentos de Ingenieria de Software #

### Repositorio y Versionado ###
A petición del docente dimos comienzo a la creación del repositoriomediante el servicio de alojamiento en web “Bitbucket”, donde vimos su facilidad de uso en comparaci ́on con otros tipos de servicios como ́este antes utilizados por nosotros, como por ejemplo: GitHub. Dentro del repositorio se podrán ver las distintas versiones del programa a lo largo de su evolución, viendo el paso a paso y destacandolas versiones que tuvieron los cambios más importantes, además de sus respectivos comentarios donde se describe cada versión del programa.
En dicho repositorio trabajé con dos ramas en específico, la rama master y la rama que nombré ¨Cambios-Chicos¨ en la cual fui subiendo versiones del programa que tenían cambios pequeños o que no conforma la última versión.

### Elicitación ###
En éste proyecto utilicé más de una técnica de elicitación ya que mi desconocimiento en el ámbito de la adopción de animales requería de investigación por ende tuve que analizar documentos como por ejemplo como es que se hacen las fichas de adopción para poder
lograr visualizar y entender el objetivo principal de el programa. A su vez tambien tuve que utilizar una tormenta de ideas para poder concretar mi visión de lo que iba a ser el programa en su producto final y canalizar pensamientos útilez que puedan servir para
brindarle una mejor experiencia al usuario.

El perfil de usuario que va a manejar mi programa sería el administrativo que tenga que gestionar las adopciones de perros por ende es muy importante que la tarea sea lo menos tediosa posible por eso se diseño un programa de fácil accesibilidad e intuitivo y tambien
que sea simplista para que se vea profesional y moderno. Se entiende a su vez que no todo administrativo segurmante tenga el mismo estilo de pantalla con la misma resolución entonces para resolver este problema busqué que el usuario pueda personalizar el tamaño
de su programa sin que se vea afectado el contenido de el mismo por eso este software es completamente responsive y flexible a las demandas del usuario.

Las tareas que va a hacer el usuario en este caso son:
-	Crear una ficha de adopción de perro
-	Buscar perros en adopción y en caso de que se quiera adopar a un perro simplemente se lo selecciona de una lista donde se almacenan todos los perros y cuando se desplieguen los detalles del perro se podrá decidir para adoptarlo. En el caso de que se quiera adoptar el perro
y se presione la opción para adoprar inmediatamente se pasará a una ventana que le permitirá mandar a un mail para contacar al dueño del perro para poder adoptarlo. Esta función funciona en tiempo real y manda mails correctamente a su destinatario permitiendo una buena
comunicación entre el usuario y el reseptor. 

### Especificación ###

Requerimientos Funcionales: 

- 	Crear ficha de adopción

-	Buscar perros en adopción

-	Contactar

-	Adoptar

-	Tener caracteristicas de los perros en adopción

-	Personas adoptantes

-	Hogar

Requerimientos No Funcionales:

-	RNF1 -Accesible (Usuario)

-	RNF2 -Performance rápida (Usuario)

-	RNF3 -Intuitivo (Usuario)

-	RNF4 -Estable (Usuario)

-	RNF5 -Usabilidad (Usuario)

-	RNF6 -Seguridad (Usuario)

-	RNF7 -Mantenibilidad (Equipo)

-	RNF8 -Desarrollado en JAVA (Equipo)

User Sories:

 1) 	El usuario quiere crear una ficha de adopción

Curso Normal:

 Actor: Usuario

 1- El usuario ingresa los datos de la persona

 2- El usuario ingresa los datos del perro

 3- El usuario hace click en el boton crear y se crea exitosamente

Curso Alternativo:

3- El usuario no ingresó una edad de la persona aceptable y salta un mensaje que despliega el error - Fin de curso alternativo

Curso Alternativo 2:

3- El usuario no llenó todos los campos obligatorios y salta un mensaje que despliega el error - Fin de curso alternativo

2) El usuario quiere buscar perros en adopción

Curso Normal:

Actor: Usuario

1- El usuario presiona la opción del menú para ver el listado de los perros

2- Aparece una lista detallada de los perros registrados

Curso Alternativo:

2- La lista está vacía entonces no aparecen resultados - Fin de curso alternativo

3) El usuario quiere mandar un mail para contactar al dueño

Curso Normal:

Actor: Usuario

1- El usuario llena todo el mensaje del mail

2- El usuario presiona el boton de enviar

Curso Alternativo:

2- El mail ingresado del destinatario es incorrecto - Fin de curso Alternativo

### Validación y verificación ###

En el momento de poner en marcha el programa supe que tenía que elegir una buena técninca de elicitación por eso utilicé documentos para basarme en la creación del programa y en el brain storming para poder
enfocarme en una experiencia única para el usuario. Una vez que pude concretar con todos esos procesos pude iniciar con el desarrollo enfocandome principalmente en una experiencia simple para el usuario
que a su vez le brindara todos los requerimientos indicados sobre lo que el programa tiene que hacer y lograr. Para esto hice un prototipo en paint con un diseño bastante básico del layout que quería lograr.

![](prototipo.png)

Una vez que tenía el prototipo me puse a buscar los colores que quería usar para la UI del programa, para eso fui a la página https://flatuicolors.com/palette que contiene una de las mejores combinaciones 
de colores para la interfaz gráfica de cualquier tipo de aplicación. Decidí usuar un azul claro para el menú de barras y un azul oscuro para el cabezal del programa. De esta manera con ésta utilización
de colores el programa no es muy invasivo para el usuario y mantengo la caracteristica de moderno que quería lograr.

![](UI1.PNG)

![](UI2.PNG)

![](UI3.PNG)

Despues de la finalización del programa hice una validacíon con familiares y amigos a ver que pensaban del software en si a lo que dijeron que era muy fácil de usar y que lograba el objetivo principal de una 
forma rápida y efectiva. Cuando les pregunté sobre la interfaz de usuario me dijeron que les habia gustado.

### Defensa y reflexión ###

Este software iría destinado para las personas que quieran agilizar la adopción de perros de una forma rapida segura y confiable, en el cual puedan ver características de la mascota como a la vez
que puedan contactar a su respectivo dueño de una forma fácil y segura.

Cuando me puse en marcha a crear este proyecto elegir una técnica de requerimiento fue una de las cosas que veía como de gran importancia ya que esto iba a influir en el resultado deseado, por lo
tanto lo primero que hice fue ponenrme en el lugar del usuario y preguntarme que es lo que quisiera tener para una aplicación de éste estilo; esto me llevo a elegir las tecnicas de requerimiento
mensionadas previamente ya que me parecieron las más indicadas para ésta situación.

La UI sin embargo fue una experiencia diferente ya que sabía exactamente a lo que quería llegar con éste proyecto y en mi punto de vista lo logré. Creé una interfaz intuitiva, moderna y prolija.
