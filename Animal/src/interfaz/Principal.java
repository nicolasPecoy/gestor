package interfaz;

import animal.Persona;
import animal.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.Panel;
import java.io.File;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Principal extends javax.swing.JFrame {

    private Sistema sistema;
    private String mailReceptor;
    ImageIcon foto;
    ImageIcon fotoTamanoCompleto;
    DefaultListModel modeloListaPerros;

    public Principal(Sistema sistema) {
        initComponents();
        setLocationRelativeTo(null);
        this.setSistema(sistema);
        modeloListaPerros = new DefaultListModel();
        listaPerros.setModel(modeloListaPerros);
        listaPerros.setCellRenderer(new ListaRender(sistema, panelListadoPerros));
        panelListadoPerros.setVisible(false);
        panelMail.setVisible(false);
    }

    public Sistema getSistema() {
        return sistema;
    }

    public void setSistema(Sistema sistema) {
        this.sistema = sistema;
    }

    public String getMailReceptor() {
        return mailReceptor;
    }

    public void setMailReceptor(String mailReceptor) {
        this.mailReceptor = mailReceptor;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelMain = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        panelBarra = new javax.swing.JPanel();
        panelOpciones = new javax.swing.JPanel();
        panelCrear = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        panelBuscar = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        panelFondo = new javax.swing.JLayeredPane();
        panelFondoFicha = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtNombreYAP = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtEdad = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCelular = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtMail = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtTrabajo = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txtNombrePerro = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtSexoCMB = new javax.swing.JComboBox<>();
        txtEdadPerro = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txtRaza = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txtTamañoCMB = new javax.swing.JComboBox<>();
        jLabel17 = new javax.swing.JLabel();
        txtPeso = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtCiudad = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txtSalud = new javax.swing.JTextField();
        fotosDePerfil = new javax.swing.JLabel();
        botonFoto = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripción = new javax.swing.JTextArea();
        btnCrear = new javax.swing.JButton();
        panelListadoPerros = new javax.swing.JPanel();
        scrollListadoAlimentos = new javax.swing.JScrollPane();
        listaPerros = new javax.swing.JList<>();
        panelOpcionesLista = new javax.swing.JPanel();
        labelFotoPerro = new javax.swing.JLabel();
        LabelTipoOtrasAnotacionesLista = new javax.swing.JLabel();
        labelNombrePerro = new javax.swing.JLabel();
        labelRazaPerro = new javax.swing.JLabel();
        labelEdadPerro = new javax.swing.JLabel();
        labelSexoPerro = new javax.swing.JLabel();
        scrollOtrasAnotacionesLista = new javax.swing.JScrollPane();
        textoOtrasAnotacionesLista = new javax.swing.JTextPane();
        btnAdoptar = new javax.swing.JButton();
        panelMail = new javax.swing.JPanel();
        txtMailMio = new javax.swing.JTextField();
        txtAsunto = new javax.swing.JTextField();
        txtEnviarA = new javax.swing.JTextField();
        btnEnviar = new javax.swing.JButton();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtMensaje = new javax.swing.JTextArea();
        jLabel25 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        panelMain.setBackground(new java.awt.Color(48, 51, 107));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Gestor de Adopción");

        javax.swing.GroupLayout panelMainLayout = new javax.swing.GroupLayout(panelMain);
        panelMain.setLayout(panelMainLayout);
        panelMainLayout.setHorizontalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelMainLayout.setVerticalGroup(
            panelMainLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMainLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
                .addContainerGap())
        );

        panelBarra.setBackground(new java.awt.Color(72, 52, 212));

        panelOpciones.setOpaque(false);
        panelOpciones.setLayout(new java.awt.GridLayout(0, 1, 0, 5));

        panelCrear.setBackground(new java.awt.Color(104, 109, 224));

        jLabel3.setBackground(new java.awt.Color(104, 109, 224));
        jLabel3.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Crear ficha de adopción");
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel3MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel3MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabel3MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelCrearLayout = new javax.swing.GroupLayout(panelCrear);
        panelCrear.setLayout(panelCrearLayout);
        panelCrearLayout.setHorizontalGroup(
            panelCrearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCrearLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE))
        );
        panelCrearLayout.setVerticalGroup(
            panelCrearLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        panelOpciones.add(panelCrear);

        panelBuscar.setBackground(new java.awt.Color(104, 109, 224));

        jLabel4.setBackground(new java.awt.Color(104, 109, 224));
        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Buscar perros en adopción");
        jLabel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel4MouseExited(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel4MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jLabel4MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout panelBuscarLayout = new javax.swing.GroupLayout(panelBuscar);
        panelBuscar.setLayout(panelBuscarLayout);
        panelBuscarLayout.setHorizontalGroup(
            panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelBuscarLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE))
        );
        panelBuscarLayout.setVerticalGroup(
            panelBuscarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
        );

        panelOpciones.add(panelBuscar);

        jLabel26.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel26.setForeground(new java.awt.Color(255, 255, 255));
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("Desarrollado por Nicolás Pecoy\n"); // NOI18N

        jLabel27.setFont(new java.awt.Font("Segoe UI", 1, 12)); // NOI18N
        jLabel27.setForeground(new java.awt.Color(255, 255, 255));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("192991"); // NOI18N

        javax.swing.GroupLayout panelBarraLayout = new javax.swing.GroupLayout(panelBarra);
        panelBarra.setLayout(panelBarraLayout);
        panelBarraLayout.setHorizontalGroup(
            panelBarraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelOpciones, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panelBarraLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelBarraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        panelBarraLayout.setVerticalGroup(
            panelBarraLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelBarraLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelOpciones, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel27)
                .addGap(31, 31, 31))
        );

        panelFondo.setOpaque(true);

        jLabel2.setText("Nombre y Apelido: ");

        jLabel5.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel5.setText("Datos Personales: ");

        jLabel6.setText("Edad:");

        txtEdad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEdadKeyTyped(evt);
            }
        });

        jLabel7.setText("Celular:");

        txtCelular.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCelularKeyTyped(evt);
            }
        });

        jLabel8.setText("Dirección:");

        jLabel9.setText("Mail:");

        jLabel10.setText("Trabajo:");

        jLabel11.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel11.setText("Datos del Perro: ");

        jLabel12.setText("Nombre: ");

        jLabel13.setText("Sexo: ");

        txtSexoCMB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Macho", "Hembra" }));
        txtSexoCMB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtSexoCMB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSexoCMBActionPerformed(evt);
            }
        });

        txtEdadPerro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEdadPerroKeyTyped(evt);
            }
        });

        jLabel14.setText("Edad:");

        jLabel15.setText("Raza:");

        jLabel16.setText("Tamaño: ");

        txtTamañoCMB.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Pequeño", "Mediano", "Grande" }));
        txtTamañoCMB.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        txtTamañoCMB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTamañoCMBActionPerformed(evt);
            }
        });

        jLabel17.setText("Peso:");

        txtPeso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPesoActionPerformed(evt);
            }
        });
        txtPeso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPesoKeyTyped(evt);
            }
        });

        jLabel18.setText("Kg");

        txtCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCiudadActionPerformed(evt);
            }
        });

        jLabel19.setText("Ciudad:");

        jLabel20.setText("Salud:");

        txtSalud.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSaludActionPerformed(evt);
            }
        });

        fotosDePerfil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/invitado_fotoPerfil.png"))); // NOI18N

        botonFoto.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        botonFoto.setText("Seleccionar foto");
        botonFoto.setBorder(null);
        botonFoto.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        botonFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonFotoActionPerformed(evt);
            }
        });

        jLabel21.setText("Descripción: ");

        txtDescripción.setColumns(20);
        txtDescripción.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        txtDescripción.setRows(5);
        jScrollPane1.setViewportView(txtDescripción);

        btnCrear.setText("Crear");
        btnCrear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelFondoFichaLayout = new javax.swing.GroupLayout(panelFondoFicha);
        panelFondoFicha.setLayout(panelFondoFichaLayout);
        panelFondoFichaLayout.setHorizontalGroup(
            panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFondoFichaLayout.createSequentialGroup()
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelFondoFichaLayout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5)
                            .addGroup(panelFondoFichaLayout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtNombreYAP, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtEdad, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, Short.MAX_VALUE)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFondoFichaLayout.createSequentialGroup()
                                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelFondoFichaLayout.createSequentialGroup()
                                        .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelFondoFichaLayout.createSequentialGroup()
                                                .addComponent(jLabel12)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtNombrePerro))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelFondoFichaLayout.createSequentialGroup()
                                                .addComponent(jLabel10)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, Short.MAX_VALUE)
                                        .addComponent(jLabel13)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtSexoCMB, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, panelFondoFichaLayout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel9)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtMail, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(panelFondoFichaLayout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(jLabel14)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtEdadPerro, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, Short.MAX_VALUE)
                                        .addComponent(jLabel15)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtRaza, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(panelFondoFichaLayout.createSequentialGroup()
                                .addComponent(jLabel16)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtTamañoCMB, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel18)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel11)
                            .addGroup(panelFondoFichaLayout.createSequentialGroup()
                                .addComponent(jLabel20)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtSalud, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelFondoFichaLayout.createSequentialGroup()
                                .addComponent(fotosDePerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panelFondoFichaLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(botonFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(panelFondoFichaLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel21)))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panelFondoFichaLayout.createSequentialGroup()
                        .addGap(354, 354, 354)
                        .addComponent(btnCrear)))
                .addContainerGap(80, Short.MAX_VALUE))
        );
        panelFondoFichaLayout.setVerticalGroup(
            panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelFondoFichaLayout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombreYAP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEdad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCelular, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 26, Short.MAX_VALUE)
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 26, Short.MAX_VALUE)
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTrabajo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 26, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addGap(18, 26, Short.MAX_VALUE)
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNombrePerro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSexoCMB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEdadPerro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtRaza, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 26, Short.MAX_VALUE)
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTamañoCMB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCiudad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 26, Short.MAX_VALUE)
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSalud, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 26, Short.MAX_VALUE)
                .addGroup(panelFondoFichaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(fotosDePerfil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonFoto, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel21)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addComponent(btnCrear)
                .addContainerGap(19, Short.MAX_VALUE))
        );

        panelListadoPerros.setOpaque(false);
        panelListadoPerros.setLayout(new java.awt.GridLayout(1, 0));

        scrollListadoAlimentos.setBackground(new java.awt.Color(255, 255, 255));
        scrollListadoAlimentos.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        scrollListadoAlimentos.setForeground(new java.awt.Color(255, 255, 255));

        listaPerros.setBackground(new java.awt.Color(254, 254, 254));
        listaPerros.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        listaPerros.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listaPerros.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        listaPerros.setSelectionBackground(new java.awt.Color(255, 190, 118));
        listaPerros.setSelectionForeground(new java.awt.Color(254, 254, 254));
        listaPerros.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listaPerrosMouseClicked(evt);
            }
        });
        scrollListadoAlimentos.setViewportView(listaPerros);

        panelListadoPerros.add(scrollListadoAlimentos);

        panelOpcionesLista.setBackground(new java.awt.Color(255, 255, 255));
        panelOpcionesLista.setOpaque(false);
        panelOpcionesLista.setLayout(null);

        labelFotoPerro.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(104, 109, 224), 2));
        panelOpcionesLista.add(labelFotoPerro);
        labelFotoPerro.setBounds(180, 50, 180, 110);

        LabelTipoOtrasAnotacionesLista.setFont(new java.awt.Font("Arial", 3, 11)); // NOI18N
        LabelTipoOtrasAnotacionesLista.setText("Otras Anotaciones:");
        panelOpcionesLista.add(LabelTipoOtrasAnotacionesLista);
        LabelTipoOtrasAnotacionesLista.setBounds(10, 140, 150, 20);

        labelNombrePerro.setBackground(new java.awt.Color(255, 255, 255));
        labelNombrePerro.setFont(new java.awt.Font("Arial", 3, 16)); // NOI18N
        labelNombrePerro.setForeground(new java.awt.Color(104, 109, 224));
        labelNombrePerro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        panelOpcionesLista.add(labelNombrePerro);
        labelNombrePerro.setBounds(10, 10, 350, 30);

        labelRazaPerro.setFont(new java.awt.Font("Arial", 3, 11)); // NOI18N
        panelOpcionesLista.add(labelRazaPerro);
        labelRazaPerro.setBounds(10, 110, 150, 20);

        labelEdadPerro.setFont(new java.awt.Font("Arial", 3, 11)); // NOI18N
        panelOpcionesLista.add(labelEdadPerro);
        labelEdadPerro.setBounds(10, 80, 150, 20);

        labelSexoPerro.setFont(new java.awt.Font("Arial", 3, 11)); // NOI18N
        panelOpcionesLista.add(labelSexoPerro);
        labelSexoPerro.setBounds(10, 50, 150, 20);

        textoOtrasAnotacionesLista.setEditable(false);
        scrollOtrasAnotacionesLista.setViewportView(textoOtrasAnotacionesLista);

        panelOpcionesLista.add(scrollOtrasAnotacionesLista);
        scrollOtrasAnotacionesLista.setBounds(10, 170, 350, 280);

        btnAdoptar.setText("Adoptar");
        btnAdoptar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdoptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnAdoptarMouseClicked(evt);
            }
        });
        btnAdoptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdoptarActionPerformed(evt);
            }
        });
        panelOpcionesLista.add(btnAdoptar);
        btnAdoptar.setBounds(160, 510, 71, 23);

        panelListadoPerros.add(panelOpcionesLista);

        txtMailMio.setEditable(false);
        txtMailMio.setForeground(new java.awt.Color(153, 153, 153));
        txtMailMio.setText("gestiondeperrosparati@gmail.com");

        txtAsunto.setEditable(false);
        txtAsunto.setText("Quiero adoptar a su perro ");
        txtAsunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtAsuntoActionPerformed(evt);
            }
        });

        btnEnviar.setText("Enviar");
        btnEnviar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEnviarActionPerformed(evt);
            }
        });

        jLabel22.setText("Mail: ");

        jLabel23.setText("Asunto: ");

        jLabel24.setText("Enviar a: ");

        txtMensaje.setColumns(20);
        txtMensaje.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        txtMensaje.setRows(5);
        txtMensaje.setText("Contestar las siguientes preguntas para que el dueño tenga mejor información:\n\nNombre completo:\n\nEdad:\n\nCelular:\n\n¿Porqué desea tener un perro?\n\n¿Es la primera vez que tiene una mascota?\n\n¿Tiene casa propia?\n\n¿Esta trabajando actualmente? Diga en que trabajo en caso de tener uno.\n\nDetalles adisionales:\n");
        jScrollPane3.setViewportView(txtMensaje);

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel25.setText("Envia mail para Adoptar:");

        javax.swing.GroupLayout panelMailLayout = new javax.swing.GroupLayout(panelMail);
        panelMail.setLayout(panelMailLayout);
        panelMailLayout.setHorizontalGroup(
            panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMailLayout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 669, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEnviar)
                    .addGroup(panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panelMailLayout.createSequentialGroup()
                            .addGroup(panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel22)
                                .addComponent(jLabel23)
                                .addComponent(jLabel24))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addGroup(panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(txtMailMio)
                                .addComponent(txtAsunto)
                                .addComponent(txtEnviarA, javax.swing.GroupLayout.PREFERRED_SIZE, 625, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(54, Short.MAX_VALUE))
        );
        panelMailLayout.setVerticalGroup(
            panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMailLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMailMio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22))
                .addGap(18, 18, 18)
                .addGroup(panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtAsunto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel23))
                .addGap(18, 18, 18)
                .addGroup(panelMailLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEnviarA, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel24))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(btnEnviar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelFondo.setLayer(panelFondoFicha, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelFondo.setLayer(panelListadoPerros, javax.swing.JLayeredPane.DEFAULT_LAYER);
        panelFondo.setLayer(panelMail, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout panelFondoLayout = new javax.swing.GroupLayout(panelFondo);
        panelFondo.setLayout(panelFondoLayout);
        panelFondoLayout.setHorizontalGroup(
            panelFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelFondoFicha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panelFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelFondoLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelListadoPerros, javax.swing.GroupLayout.DEFAULT_SIZE, 770, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(panelFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelMail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelFondoLayout.setVerticalGroup(
            panelFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelFondoFicha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(panelFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(panelFondoLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(panelListadoPerros, javax.swing.GroupLayout.DEFAULT_SIZE, 549, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(panelFondoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(panelMail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelBarra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panelFondo))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panelMain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelBarra, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelFondo)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseEntered
        panelCrear.setBackground(new Color(255, 190, 118));
    }//GEN-LAST:event_jLabel3MouseEntered

    private void jLabel3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseExited
        panelCrear.setBackground(new Color(104, 109, 224));
    }//GEN-LAST:event_jLabel3MouseExited

    private void jLabel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseEntered
        panelBuscar.setBackground(new Color(255, 190, 118));
    }//GEN-LAST:event_jLabel4MouseEntered

    private void jLabel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseExited
        panelBuscar.setBackground(new Color(104, 109, 224));
    }//GEN-LAST:event_jLabel4MouseExited

    private void jLabel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MousePressed
        panelCrear.setBackground(new Color(235, 77, 75));
    }//GEN-LAST:event_jLabel3MousePressed

    private void jLabel3MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseReleased
        panelCrear.setBackground(new Color(255, 190, 118));
    }//GEN-LAST:event_jLabel3MouseReleased

    private void jLabel4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MousePressed
        panelBuscar.setBackground(new Color(235, 77, 75));
    }//GEN-LAST:event_jLabel4MousePressed

    private void jLabel4MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseReleased
        panelBuscar.setBackground(new Color(255, 190, 118));
    }//GEN-LAST:event_jLabel4MouseReleased

    private void listaPerrosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listaPerrosMouseClicked
        if (!listaPerros.isSelectionEmpty()) {
            togglePanelAlimentosInformacion();
        }
    }//GEN-LAST:event_listaPerrosMouseClicked

    private void txtSexoCMBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSexoCMBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSexoCMBActionPerformed

    private void txtTamañoCMBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTamañoCMBActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTamañoCMBActionPerformed

    private void txtPesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPesoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPesoActionPerformed

    private void txtCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCiudadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCiudadActionPerformed

    private void txtSaludActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSaludActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSaludActionPerformed

    private void botonFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonFotoActionPerformed
        importarFoto();
    }//GEN-LAST:event_botonFotoActionPerformed

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
        panelListadoPerros.setVisible(false);
        panelFondoFicha.setVisible(true);
        panelMail.setVisible(false);
        modeloListaPerros.clear();
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        panelFondoFicha.setVisible(false);
        modeloListaPerros.clear();
        panelListadoPerros.setVisible(true);
        panelMail.setVisible(false);
        panelOpcionesLista.setVisible(false);
        agregarValoresListaAlimentos();
    }//GEN-LAST:event_jLabel4MouseClicked

    private void btnAdoptarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAdoptarMouseClicked
        panelListadoPerros.setVisible(false);
        panelFondoFicha.setVisible(false);
        panelMail.setVisible(true);
    }//GEN-LAST:event_btnAdoptarMouseClicked

    private void btnCrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearActionPerformed
        if (txtNombreYAP.getText().isEmpty() || txtEdad.getText().isEmpty() || txtCelular.getText().isEmpty() || txtDireccion.getText().isEmpty() || txtDireccion.getText().isEmpty() || txtMail.getText().isEmpty() || txtNombrePerro.getText().isEmpty() || txtEdadPerro.getText().isEmpty() || txtRaza.getText().isEmpty() || txtPeso.getText().isEmpty() || txtCiudad.getText().isEmpty() || txtSalud.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Los campos no pueden estar vacios!", "Error", 0);
        } else if (Integer.parseInt(txtEdad.getText()) < 18 || Integer.parseInt(txtEdad.getText()) > 100) {
            JOptionPane.showMessageDialog(this, "Ingrese una edad aceptable!", "Error", 0);
        } else {

            String nombre = txtNombreYAP.getText();
            int edad = Integer.parseInt(txtEdad.getText());
            int celular = Integer.parseInt(txtCelular.getText());
            String direccion = txtDireccion.getText();
            String mail = txtMail.getText();
            String trabajo = txtTrabajo.getText();
            Persona p = new Persona(nombre, edad, direccion, celular, trabajo, mail);
            String nombrePerro = txtNombrePerro.getText();
            String sexo = txtSexoCMB.getItemAt(txtSexoCMB.getSelectedIndex());
            int edadPerro = Integer.parseInt(txtEdadPerro.getText());
            String raza = txtRaza.getText();
            String tamano = txtTamañoCMB.getItemAt(txtTamañoCMB.getSelectedIndex());
            String peso = txtPeso.getText();
            String ciudad = txtCiudad.getText();
            String salud = txtSalud.getText();
            String descripcion = txtDescripción.getText();
            Perro perro = new Perro(nombrePerro, sexo, raza, edadPerro, tamano, peso, ciudad, salud, descripcion, false, fotoTamanoCompleto, fotoTamanoCompleto, p);
            sistema.agregarPerro(perro);
            JOptionPane.showMessageDialog(this, "Se ha registrado exitosamente!", "Aviso", 0);
        }
    }//GEN-LAST:event_btnCrearActionPerformed

    private void txtEdadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEdadKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
    }//GEN-LAST:event_txtEdadKeyTyped

    private void txtCelularKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCelularKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
    }//GEN-LAST:event_txtCelularKeyTyped

    private void txtEdadPerroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEdadPerroKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
    }//GEN-LAST:event_txtEdadPerroKeyTyped

    private void txtPesoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesoKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
    }//GEN-LAST:event_txtPesoKeyTyped

    private void txtAsuntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtAsuntoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtAsuntoActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            this.getSistema().getPersistencia().writeObject(this.getSistema(), "SavedData.dat");
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_formWindowClosing

    private void btnAdoptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdoptarActionPerformed
        txtEnviarA.setText(this.getMailReceptor());
    }//GEN-LAST:event_btnAdoptarActionPerformed

    private void btnEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEnviarActionPerformed
        enviarMail();
    }//GEN-LAST:event_btnEnviarActionPerformed

    public void importarFoto() {
        JFileChooser file = new JFileChooser();
        file.setCurrentDirectory(new File(System.getProperty("user.home")));
        //filtra los archivos que puede elegir
        FileNameExtensionFilter filter = new FileNameExtensionFilter("*.Images", "jpg", "gif", "png");
        file.addChoosableFileFilter(filter);
        int result = file.showSaveDialog(null);
        //cuando el usuario apreta en guardar en el File Chooser
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = file.getSelectedFile();
            String path = selectedFile.getAbsolutePath();
            if (panelCrear.isVisible()) {
                fotosDePerfil.setIcon(modificarTamañoFoto(path));
            }
            /*else {
                fotoDePerfilP.setIcon(modificarTamañoFoto(path));
            }*/
        } //cuando el usuario apreta en guardar en el File Chooser
        else if (result == JFileChooser.CANCEL_OPTION) {
            System.out.println("No File Select");
        }
    }

    public void agregarValoresListaAlimentos() {
        String nombre;
        if (panelListadoPerros.isVisible()) {
            for (Perro perro : sistema.getListaPerros()) {
                nombre = perro.getNombre();
                modeloListaPerros.addElement(nombre);
            }
        }
    }

    public void enviarMail() {
        Properties propiedad = new Properties();
        propiedad.setProperty("mail.smtp.host", "smtp.gmail.com");
        propiedad.setProperty("mail.smtp.starttls.enable", "true");
        propiedad.setProperty("mail.smtp.port", "587");

        Session sesion = Session.getDefaultInstance(propiedad);
        String correoEnvia = "gestiondeperrosparati@gmail.com";
        String contrasena = "IngenieriaDeSoftware1";
        if (txtEnviarA.getText().isEmpty() || txtMensaje.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Debe ingresar todos los datos!", "Error", WIDTH);
        } else {
            String receptor = txtEnviarA.getText();
            String asunto = txtAsunto.getText();
            String mensaje = txtMensaje.getText();

            MimeMessage mail = new MimeMessage(sesion);
            try {
                mail.setFrom(new InternetAddress(correoEnvia));
                mail.addRecipient(Message.RecipientType.TO, new InternetAddress(receptor));
                mail.setSubject(asunto);
                mail.setText(mensaje);

                Transport transportar = sesion.getTransport("smtp");
                transportar.connect(correoEnvia, contrasena);
                transportar.sendMessage(mail, mail.getRecipients(Message.RecipientType.TO));
                transportar.close();

                JOptionPane.showMessageDialog(null, "Mail enviado! En poco tiempo podrá adoptar el perro!");

            } catch (AddressException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(Panel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ImageIcon modificarTamañoFoto(String ImagePath) {
        ImageIcon MyImage = new ImageIcon(ImagePath);
        Image img = MyImage.getImage();
        Image imgPerfil = img.getScaledInstance(90, 90, Image.SCALE_SMOOTH);
        ImageIcon imagenCompleta = new ImageIcon(imgPerfil);
        fotoTamanoCompleto = imagenCompleta;
        Image newImg = img.getScaledInstance(fotosDePerfil.getWidth(), fotosDePerfil.getHeight(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(newImg);
        foto = image;
        return image;
    }

    public void togglePanelAlimentosInformacion() {
        if (!panelOpcionesLista.isVisible()) {
            panelOpcionesLista.setVisible(true);
            agregarDatosAlimento();
        } else {
            agregarDatosAlimento();
        }
    }

    public void agregarDatosAlimento() {
        int indice = listaPerros.getSelectedIndex();
        sistema.getListaPerros().get(indice).getNombre();
        labelNombrePerro.setText(sistema.getListaPerros().get(indice).getNombre());
        labelSexoPerro.setText("Sexo: " + sistema.getListaPerros().get(indice).getSexo());
        labelEdadPerro.setText("Edad: " + sistema.getListaPerros().get(indice).getEdad());
        labelRazaPerro.setText("Raza: " + sistema.getListaPerros().get(indice).getRaza());
        textoOtrasAnotacionesLista.setText(sistema.getListaPerros().get(indice).getDescripcion());
        this.setMailReceptor(sistema.getListaPerros().get(indice).getPersona().getMail());
        Image imagen = sistema.getListaPerros().get(indice).getFotoPerfil().getImage();
        ImageIcon nuevaImagen = new ImageIcon(imagen.getScaledInstance(labelFotoPerro.getWidth(), labelFotoPerro.getHeight(), Image.SCALE_SMOOTH));
        labelFotoPerro.setIcon(nuevaImagen);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LabelTipoOtrasAnotacionesLista;
    private javax.swing.JButton botonFoto;
    private javax.swing.JButton btnAdoptar;
    private javax.swing.JButton btnCrear;
    private javax.swing.JButton btnEnviar;
    private javax.swing.JLabel fotosDePerfil;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel labelEdadPerro;
    private javax.swing.JLabel labelFotoPerro;
    private javax.swing.JLabel labelNombrePerro;
    private javax.swing.JLabel labelRazaPerro;
    private javax.swing.JLabel labelSexoPerro;
    private javax.swing.JList<String> listaPerros;
    private javax.swing.JPanel panelBarra;
    private javax.swing.JPanel panelBuscar;
    private javax.swing.JPanel panelCrear;
    private javax.swing.JLayeredPane panelFondo;
    private javax.swing.JPanel panelFondoFicha;
    private javax.swing.JPanel panelListadoPerros;
    private javax.swing.JPanel panelMail;
    private javax.swing.JPanel panelMain;
    private javax.swing.JPanel panelOpciones;
    private javax.swing.JPanel panelOpcionesLista;
    private javax.swing.JScrollPane scrollListadoAlimentos;
    private javax.swing.JScrollPane scrollOtrasAnotacionesLista;
    private javax.swing.JTextPane textoOtrasAnotacionesLista;
    private javax.swing.JTextField txtAsunto;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtCiudad;
    private javax.swing.JTextArea txtDescripción;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JTextField txtEdad;
    private javax.swing.JTextField txtEdadPerro;
    private javax.swing.JTextField txtEnviarA;
    private javax.swing.JTextField txtMail;
    private javax.swing.JTextField txtMailMio;
    private javax.swing.JTextArea txtMensaje;
    private javax.swing.JTextField txtNombrePerro;
    private javax.swing.JTextField txtNombreYAP;
    private javax.swing.JTextField txtPeso;
    private javax.swing.JTextField txtRaza;
    private javax.swing.JTextField txtSalud;
    private javax.swing.JComboBox<String> txtSexoCMB;
    private javax.swing.JComboBox<String> txtTamañoCMB;
    private javax.swing.JTextField txtTrabajo;
    // End of variables declaration//GEN-END:variables
}
