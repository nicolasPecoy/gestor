package interfaz;

import animal.*;
import java.awt.*;
import javax.swing.*;

public class ListaRender extends JLabel implements ListCellRenderer {

    JPanel panelPerros;

    Sistema sistema;
    ImageIcon foto = new ImageIcon();

    public ListaRender(Sistema unSistema, JPanel unPanelPerro) {
        this.sistema = unSistema;
        this.panelPerros = unPanelPerro;

    }

    ListaRender(Sistema sistema, boolean b, boolean b0, boolean visible, boolean visible0) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Component getListCellRendererComponent(JList list, Object valor, int i, boolean isSelected, boolean bln1) {
        Image imagen;
        String nombre = valor.toString();
        setText(nombre);

        if (panelPerros != null && panelPerros.isVisible()) {
            for (Perro listaPerros : sistema.getListaPerros()) {
                foto = listaPerros.getFotoParaListas();
                imagen = foto.getImage();
                if (!nombre.equals("") && listaPerros.getNombre().equals(sistema.getListaPerros().get(i).getNombre())) {
                    foto.setImage(imagen.getScaledInstance(150, 90, Image.SCALE_SMOOTH));
                    this.setIcon(foto);
                }
            }
        }

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }
        setEnabled(list.isEnabled());
        setFont(list.getFont());
        setOpaque(true);
        return this;
    }
}
