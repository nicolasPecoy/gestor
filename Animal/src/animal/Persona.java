package animal;

import java.io.Serializable;

public class Persona implements Serializable{

    private String nombreApellido;
    private int edad;
    private String direccion;
    private int celular;
    private String trabajo;
    private String mail;

    public Persona() {
    }

    public Persona(String nombreApellido, int edad, String direccion, int celular, String trabajo, String mail) {
        this.nombreApellido = nombreApellido;
        this.edad = edad;
        this.direccion = direccion;
        this.celular = celular;
        this.trabajo = trabajo;
        this.mail = mail;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getNombreApellido() {
        return nombreApellido;
    }

    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    public String getTrabajo() {
        return trabajo;
    }

    public void setTrabajo(String trabajo) {
        this.trabajo = trabajo;
    }

    public boolean equals(Object o) {
        return this.getNombreApellido().equals(o);
    }
}
