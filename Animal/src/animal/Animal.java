package animal;

import interfaz.Principal;
import java.io.IOException;
import java.io.Serializable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Animal implements Serializable {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Sistema sistema = new Sistema();
        sistema = (Sistema) sistema.getPersistencia().readObject(sistema,"SavedData.dat");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException e) {
        }
        Principal pr = new Principal(sistema);
        pr.setVisible(true);
    }
    
}
