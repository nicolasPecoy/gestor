package animal;

import java.io.Serializable;
import java.util.*;
import persistensia.Persistencia;

public class Sistema implements Serializable {

    
    private Persistencia persistencia;
    private ArrayList <Perro> listaPerros;
    
    public Sistema() {
        persistencia = new Persistencia();
        listaPerros = new ArrayList<>();
        
    }

    public ArrayList<Perro> getListaPerros() {
        return listaPerros;
    }

    public void setListaPerros(ArrayList<Perro> listaPerros) {
        this.listaPerros = listaPerros;
    }
    public void agregarPerro(Perro unPerro) {
        this.getListaPerros().add(unPerro);
    }

    public Persistencia getPersistencia() {
        return persistencia;
    }

    public void setPersistencia(Persistencia persistencia) {
        this.persistencia = persistencia;
    }
}
