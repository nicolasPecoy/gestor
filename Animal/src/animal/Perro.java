package animal;

import java.awt.Toolkit;
import java.io.Serializable;
import javax.swing.ImageIcon;

public class Perro implements Serializable{
    private String nombre;
    private String sexo;
    private int edad;
    private String raza;
    private String tamano;
    private String peso;
    private String ciudad;
    private String salud;
    private String descripcion;
    private boolean adoptado;
    private ImageIcon fotoPerfil;
    private ImageIcon fotoParaListas;
    private Persona persona;
    
    public Perro() {
        this.setNombre("No especificado");
        this.setSexo("No especificado");
        this.setRaza("No especificado");
        this.setEdad(0);
        this.setTamano("No especificado");
        this.setPeso("No especificado");
        this.setCiudad("No especificado");
        this.setSalud("No especificado");
        this.setDescripcion("No especificado");
        this.setAdoptado(false);
        fotoPerfil = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/invitado_fotoPerfil.png")));
        fotoParaListas = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/imagenes/invitado_fotoPerfil.png")));
    }

    public Perro(String unNombre, String unSexo, String unaRaza, int unaEdad, String unTamano, String unPeso, String unaCiudad , String salud, String unaDescripcion , boolean estaAdoptado,ImageIcon unaImagen , ImageIcon unaImagenLista, Persona persona) {
        this.setNombre(unNombre);
        this.setSexo(unSexo);
        this.setRaza(unaRaza);
        this.setEdad(unaEdad);
        this.setTamano(unTamano);
        this.setPeso(unPeso);
        this.setCiudad(unaCiudad);
        this.setSalud(salud);
        this.setDescripcion(unaDescripcion);
        this.setAdoptado(estaAdoptado);
        this.setFotoPerfil(unaImagen);
        this.setFotoParaListas(unaImagenLista);
        this.setPersona(persona);
    }

    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        this.nombre = unNombre;
    }


    public ImageIcon getFotoPerfil() {
        return fotoPerfil;
    }

    public void setFotoPerfil(ImageIcon fotoPerfil) {
        this.fotoPerfil = fotoPerfil;
    }

    public ImageIcon getFotoParaListas() {
        return fotoParaListas;
    }

    public void setFotoParaListas(ImageIcon fotoParaListas) {
        this.fotoParaListas = fotoParaListas;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getTamano() {
        return tamano;
    }

    public void setTamano(String tamano) {
        this.tamano = tamano;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getSalud() {
        return salud;
    }

    public void setSalud(String salud) {
        this.salud = salud;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isAdoptado() {
        return adoptado;
    }

    public void setAdoptado(boolean adoptado) {
        this.adoptado = adoptado;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    /*@Override
    public String toString() {
    return "\nNombre: \n" + this.getNombre() + "\nTipo de alimento: \n" + this.getTipo()
    + "\nPrincipales nutrientes: \n" + this.getNutrientes() + "\nProporción: \n" + this.getProporcion();
    }*/
    
}
