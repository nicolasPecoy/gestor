package persistensia;

import animal.Sistema;
import java.io.*;
import javax.swing.JOptionPane;

public class Persistencia implements Serializable {

    public Persistencia() {

    }

    //Guarda los jugadores registrados
    public void writeObject(Sistema unSistema, String archivo) throws IOException, ClassNotFoundException {
        try {
            FileOutputStream f = new FileOutputStream(archivo);
            BufferedOutputStream b = new BufferedOutputStream(f);
            ObjectOutputStream o = new ObjectOutputStream(b);
            o.writeObject(unSistema);
            o.flush();
            o.close();

        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No se encuentra el archivo", "ERROR", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {

        }
    }

    //Se muestran los jugadores guardados
    public Object readObject(Sistema sistema, String archivo)
            throws IOException, ClassNotFoundException {
        Object unSistema = null;
        try {
            FileInputStream f = new FileInputStream(archivo);
            BufferedInputStream b = new BufferedInputStream(f);
            ObjectInputStream s = new ObjectInputStream(b);
            unSistema = s.readObject();

        } catch (FileNotFoundException ex) {
            writeObject(sistema, "SavedData.dat");
            unSistema = sistema;
        } catch (IOException ex) {
            writeObject(sistema, "SavedData.dat");
            unSistema = sistema;
        }
        return unSistema;
    }
}
